package contracts

import (
	"fmt"
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/health-certificate/model"

)

// ReadCertificate returns the asset stored in the world state with given id.
func (s *SmartContract) ReadCertificate(ctx contractapi.TransactionContextInterface, id string) (*model.Certificate, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	// Get ID of submitting client identity
	ocert, err := getOpenCert(ctx, id, json)
	if err != nil {
		return nil, err
	}
	clientOrgMsp, err := getSubmittingClientOrgMsp(ctx)
	// check if client org is marked as recipient
	if !contains(ocert.Recipients, clientOrgMsp) {
		return nil, fmt.Errorf("no access is given to read %s, use open data instead", id)
	}
	collection := getCollectionName()
	certJSON, err := ctx.GetStub().GetPrivateData(collection, id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from private data %s", err.Error())
	}
	if certJSON == nil {
		return nil, fmt.Errorf("no access is given to read %s, use open data instead", id)
	}
	var cert model.Certificate
	err = json.Unmarshal(certJSON, &cert)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal private data %s", err.Error())
	}
	return &cert, nil
}
