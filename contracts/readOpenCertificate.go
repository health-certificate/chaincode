package contracts

import (
	"github.com/hyperledger/fabric-contract-api-go/contractapi"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/health-certificate/model"

)

// ReadOpenCertificate returns the asset stored in the world state with given id.
func (s *SmartContract) ReadOpenCertificate(ctx contractapi.TransactionContextInterface, id string) (*model.Payload, error) {
	var json = jsoniter.ConfigCompatibleWithStandardLibrary
	return getOpenCert(ctx, id, json)
}

