ARG GO_VER=1.16.7
ARG ALPINE_VER=3.14

FROM alpine:${ALPINE_VER} as base

RUN apk add -U --no-cache ca-certificates tzdata

# Add unprivileged user
RUN adduser -s /bin/true -u 1000 -D -h /app app \
    && sed -i -r "/^(app|root)/!d" /etc/group /etc/passwd \
    && sed -i -r 's#^(.*):[^:]*$#\1:/sbin/nologin#' /etc/passwd


FROM golang:${GO_VER}-alpine${ALPINE_VER} as build
# Add CA certificates and timezone data files
RUN apk add -U --no-cache ca-certificates tzdata
WORKDIR /go/src/gitlab.com/health-certificate/chaincode
COPY . .

RUN go get -d -v ./...
# Build application
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -trimpath -ldflags '-extldflags "-static" -w -s' -o health-cert-chaincode -v .

# Production ready image
# Pass the binary to the prod image
FROM scratch

# Add the timezone data files
COPY --from=base /usr/share/zoneinfo /usr/share/zoneinfo

# Add the CA certificates
COPY --from=base /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

# Add-in our unprivileged user
COPY --from=base /etc/passwd /etc/group /etc/shadow /etc/

COPY --from=build /go/src/gitlab.com/health-certificate/chaincode/health-cert-chaincode /app/chaincode
WORKDIR /app
EXPOSE 9999
USER app
ENTRYPOINT ["./chaincode"]
