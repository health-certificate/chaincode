module gitlab.com/health-certificate/chaincode

go 1.16

require (
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20210718160520-38d29fabecb9
	github.com/hyperledger/fabric-contract-api-go v1.1.1
	github.com/json-iterator/go v1.1.11
	gitlab.com/health-certificate/model v0.0.0-20210808082006-f8ba43dc0854
)
